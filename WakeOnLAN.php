<?php

//namespace PHPGangsta;

class WakeOnLAN
{
    public static function wakeUp($macAddressHexadecimal, $broadcastAddress, $port)
    {
        $macAddressHexadecimal = str_replace(':', '', $macAddressHexadecimal);

        // check if $macAddress is a valid mac address
        if (!ctype_xdigit($macAddressHexadecimal)) {
            throw new Exception('Mac address invalid, only 0-9 and a-f are allowed');
        }

        $macAddressBinary = pack('H12', $macAddressHexadecimal);

        $magicPacket = str_repeat(chr(0xff), 6).str_repeat($macAddressBinary, 16);

		//check if $port is 7 or 9
		if ( ($port != 7) && ($port != 9) ) {
			throw new Exception("Port must be 7 or 9", 99);
		}

        if (!$fp = fsockopen('udp://' . $broadcastAddress, $port, $errno, $errstr, 2)) {
            throw new Exception("Cannot open UDP socket: {$errstr}", $errno);
        }
        fputs($fp, $magicPacket);
        fclose($fp);
    }
}

?>