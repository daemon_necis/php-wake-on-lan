<!DOCTYPE html>
<html lang="en">
  <meta charset="UTF-8">
  <title>Wake me up</title>
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <style>
    body,html {
    	width:100%;
    	height:100%;
    	display: flex;
		justify-content: center;
		align-items: center;
    }
    div.outer {
		width:90%;
    	height:90%;
    	margin 0 auto;
    	background-color:gray;
    	border:5px solid lightgrey;
		display: flex;
		align-items: center;
		justify-content: center;
    }
    div.inner {
    	width:100%;
    }
    div.spacer {
    	width:100%;
    	height:25px;
    }
    form {
    	text-align: center;
    }
    input {
    	height:50px;
    	width: 80%;
    }
	input {
		padding: 15px 25px;
		font-size: 24px;
		text-align: center;
		cursor: pointer;
		outline: none;
		color: #fff;
		background-color: #04AA6D;
		border: none;
		border-radius: 15px;
		box-shadow: 0 9px #999;
	}

	input:hover {
		background-color: #3e8e41
	}

	input:active {
		background-color: #3e8e41;
		box-shadow: 0 5px #666;
		transform: translateY(4px);
	}
  </style>
  <body>
    <div class="outer">
    	<div class="inner">
			<div class="spacer"></div>
			<form method="POST" action="index.php">
   				<input type="submit" name="desktop" value="Wake up &quot;Desktop&quot;" /><br/>
   			</form>
   			<div class="spacer"></div>
   		</div>
    </div>

    <?php
		require_once('WakeOnLAN.php');
		
		//phpinfo();
   	
    	if ( ($_SERVER['REQUEST_METHOD'] == "POST") and ( array_key_exists('desktop', $_POST) ) ) {
			WakeOnLAN::wakeUp('A8:A1:59:70:3D:D8', '192.168.10.255', 7);
			WakeOnLAN::wakeUp('A8:A1:59:70:3D:D8', '192.168.10.255', 9);
		};
    ?>
	

  </body>
</html>